import logging
import os
import sys
from logging.handlers import TimedRotatingFileHandler

from flask import Flask
from flask_appbuilder.menu import Menu
from flask_appbuilder.security.mongoengine.manager import SecurityManager
from flask_appbuilder import AppBuilder
from flask_mongoengine import MongoEngine

"""
 Logging configuration
"""


app = Flask(__name__)
app.config.from_object('config')
db = MongoEngine(app)
appbuilder = AppBuilder(app, security_manager_class=SecurityManager, menu=Menu(reverse=False))

conf = app.config

logging.basicConfig(format=conf.get('LOG_FORMAT'))
logging.getLogger().setLevel(logging.DEBUG if app.debug else conf.get('PROD_LOG_LEVEL'))

# logging is not correctly - TODO:  take a break
if not app.debug:
    path_to_logs = 'logs'
    try:
        os.makedirs(path_to_logs)
    except OSError:
        pass

    filename = path_to_logs + '/all.log'
    # set file rotate
    trf_handler = TimedRotatingFileHandler(filename, when='MIDNIGHT', backupCount=30)
    trf_handler.setLevel(conf.get('PROD_LOG_LEVEL'))
    logging.getLogger().addHandler(trf_handler)

    handler = logging.FileHandler(path_to_logs + '/errors.log')  # errors logged to this file
    handler.setFormatter(logging.Formatter(conf.get('LOG_FORMAT')))
    handler.setLevel(logging.ERROR)  # only log errors and above
    app.logger.addHandler(handler)  # attach the handler to the app's logger



from app.views import system_views
from app.views import scriptable_messages_views
from app.views import product_views
