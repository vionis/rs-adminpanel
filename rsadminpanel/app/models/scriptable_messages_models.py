from mongoengine import Document
from mongoengine import StringField, ReferenceField, IntField, GeoPointField

from app.models.product_models import ProductType

"""
Define you MongoEngine Models here
"""


class ScriptableMessageCondition(Document):
    name = StringField(max_length=60, required=True)
    description = StringField(max_length=60)
    score_reached = IntField()
    time_reached = IntField()  # reached days register acc
    count_buys_reached = IntField()
    geo_reached = GeoPointField()

    product_type_reached_type = ReferenceField(ProductType)
    product_type_reached_count = IntField()

    product_reached_type = ReferenceField(ProductType)
    product_reached_count = IntField()

    def __unicode__(self):
        return self.name
