# coding=utf-8
from flask_appbuilder.security.mongoengine.models import User
from mongoengine import Document
from mongoengine import StringField, ReferenceField


class RestaurantDBModel(Document):
    name = StringField(max_length=60, required=True, unique=True)
    description = StringField(max_length=60, required=True)

    def __unicode__(self):
        return self.name


class RestaurantSubject(Document):
    name = StringField(max_length=60, required=True, unique=True)
    description = StringField(max_length=60)
    restaurant = ReferenceField(RestaurantDBModel, required=True)

    def __unicode__(self):
        return self.name


class UserRestaurant(Document):
    """
    User and restaurant coupling
    """

    user = ReferenceField(User, required=True)
    restaurant = ReferenceField(RestaurantDBModel, required=True)

    def __unicode__(self):
        return self.user.email
