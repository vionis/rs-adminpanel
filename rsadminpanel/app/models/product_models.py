# coding=utf-8
from mongoengine import Document
from mongoengine import StringField, ReferenceField, ListField

from app.models.system_models import RestaurantSubject, RestaurantDBModel


class ProductType(Document):
    # ----- required to restaurant reference
    restaurant = ReferenceField(RestaurantDBModel)
    restaurant_subjects = ListField(ReferenceField(RestaurantSubject), required=True)
    # -----

    name = StringField(max_length=60, required=True)

    def __unicode__(self):
        return self.name
