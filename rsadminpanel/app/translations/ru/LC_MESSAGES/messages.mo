��          �               L  	   M  
   W     b     n     �     �     �     �     �     �     �     �  5   �     1  	   J  "   T     w  &   �  �  �  !   �     �     �  `   �     G     e  O   x  4   �  &   �  $   $  $   I     n     }  -        �     �     �  T   �   Added Row Audit Info Changed Row Copy the selected roles? Deleted Row Detail Edit User Information Failed login count No matches found No row selected Page not found Profile The user's email, this will also be used for OID auth User confirmation needed User info Write the user first name or names Write the user last name You sure you want to delete this item? Project-Id-Version: PROJECT VERSION
Report-Msgid-Bugs-To: EMAIL@ADDRESS
POT-Creation-Date: 2018-10-10 18:00+0300
PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE
Last-Translator: FULL NAME <EMAIL@ADDRESS>
Language: ru
Language-Team: ru <LL@li.org>
Plural-Forms: nplurals=3; plural=(n%10==1 && n%100!=11 ? 0 : n%10>=2 && n%10<=4 && (n%100<10 || n%100>=20) ? 1 : 2)
MIME-Version: 1.0
Content-Type: text/plain; charset=utf-8
Content-Transfer-Encoding: 8bit
Generated-By: Babel 2.6.0
 Успешно добавлено Статистика Успешно изменено Вы действительно хотите скопировать выбранные роли? Успешно удалено Подробнее Изменить основную информацию пользователя Количество неудачных входов Элементов не найдено Не выбраны элементы Страница не найдена Профиль   Требуется подтверждение Общая информация     Вы действительно хотите удалить этот элемент? 