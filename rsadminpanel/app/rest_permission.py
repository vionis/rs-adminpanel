# coding=utf-8
from flask import g

from flask_appbuilder.models.mongoengine.filters import FilterEqualFunction

from app.helpers import list_to_str
from app.models.system_models import UserRestaurant, RestaurantSubject
from app.views.system_views import BaseModelView


def get_rel_rest_for_current_user():
    rel_restaurant = next(UserRestaurant.objects(user=g.user.id), None)
    if not rel_restaurant:
        return ''

    return rel_restaurant.restaurant.id


class UserFilter(FilterEqualFunction):
    name = "UserFilterInRestPermission"

    def apply(self, query, func):
        result = func()

        if not result:
            return query

        return super(UserFilter, self).apply(query, lambda: result)


class BaseRestaurantViewModel(BaseModelView):
    # name
    label_columns = {'restaurant_subjects': u'Заведения'}
    description_columns = {'restaurant_subjects': u'Оставьте пустым, если применяется для всех заведений'}

    # format
    list_columns = ['restaurant_subjects']
    formatters_columns = {'restaurant_subjects': lambda x: list_to_str(x)}

    # logic
    base_filters = [['restaurant', UserFilter, get_rel_rest_for_current_user]]
    query_rel_fields = {'restaurant_subjects': [['restaurant', UserFilter, get_rel_rest_for_current_user]]}
    add_form_query_rel_fields = query_rel_fields
    edit_form_query_rel_fields = query_rel_fields
    search_form_query_rel_fields = query_rel_fields

    # excludes
    exclude_columns = ['restaurant']
    add_exclude_columns = exclude_columns
    edit_exclude_columns = exclude_columns
    show_exclude_columns = exclude_columns
    search_exclude_columns = exclude_columns

    def __init__(self, **kwargs):
        self.pre_init()
        super(BaseRestaurantViewModel, self).__init__(**kwargs)

    def pre_update(self, item):
        self.fill_rest_subjects_and_set_rest(item)

    def pre_add(self, item):
        self.fill_rest_subjects_and_set_rest(item)

    @staticmethod
    def fill_rest_subjects_and_set_rest(item):
        # if not selected rests - fill in all related
        if not item.restaurant_subjects:
            # get related rest
            rel_restaurant = next(UserRestaurant.objects(user=g.user.id), None)

            # if rel_restaurant is null - return
            if not rel_restaurant:
                return

            # fill
            item.restaurant_subjects = RestaurantSubject.objects(restaurant=rel_restaurant.restaurant.id)

        # set restaurant - need it to show all assigned to users rests
        item.restaurant = item.restaurant_subjects[0].restaurant

    def pre_init(self):
        """
        Override this to pre_init other fields here
        """
        pass

