
def list_to_str(l):
    return '[' + ', '.join('\'' + str(x) + '\'' for x in l) + ']'