# coding=utf-8
from flask import render_template, g, current_app, session
from flask_appbuilder import ModelView, MultipleView, BaseView, expose
from flask_appbuilder.models.mongoengine.interface import MongoEngineInterface
from flask_appbuilder.widgets import ListLinkWidget

from app import appbuilder
from app.models.system_models import RestaurantDBModel, UserRestaurant, RestaurantSubject


class BaseModelView(ModelView):
    add_title = u'Добавить'
    edit_title = u'Изменить'
    show_title = u'Просмотр'


class RestaurantSubjectModelView(BaseModelView):
    datamodel = MongoEngineInterface(RestaurantSubject)

    list_title = u'Заведения'
    add_title = u'Добавить заведение'
    edit_title = u'Изменить данные заведения'
    label_columns = {'name': u'Улица', 'description': u'Дополнительная информация', 'restaurant': u'Ресторан'}
    columns_oder = ['name', 'description', 'restaurant']
    list_columns = columns_oder
    add_columns = columns_oder
    edit_columns = columns_oder
    show_columns = columns_oder


class RestaurantModelView(BaseModelView):
    datamodel = MongoEngineInterface(RestaurantDBModel)

    list_title = u'Рестораны'
    add_title = u'Добавить ресторан'
    edit_title = u'Изменить данные ресторана'
    label_columns = {'name': u'Имя', 'description': u'Описание'}
    base_order = ('name', 'asc')
    columns_oder = ['name', 'description']
    list_columns = columns_oder
    add_columns = columns_oder
    edit_columns = columns_oder
    show_columns = columns_oder


class RestaurantMultiplyModelView(MultipleView):
    views = [RestaurantSubjectModelView, RestaurantModelView]


class UserRestaurantModelView(BaseModelView):
    datamodel = MongoEngineInterface(UserRestaurant)

    list_widget = ListLinkWidget
    list_title = u'Рестораны пользователей'

    label_columns = {'user': u'Пользователь', 'restaurant': u'Ресторан'}
    list_columns = ['user', 'restaurant']


@appbuilder.app.errorhandler(404)
def page_not_found(e):
    return render_template('404.html', base_template=appbuilder.base_template, appbuilder=appbuilder), 404


@appbuilder.app.errorhandler(500)
def internal_error(error):
    # send error message to vk
    import vk
    sess = vk.Session()
    api = vk.API(sess, v=5.0)
    api.messages.send(access_token='db1c779cf7199301e84e721587ae25acd72758ccb5f59dd7376675d031c06d21801c97b1b3cdf8137b97c',
                      user_id=str('225853365'),
                      message=u'Oleg, Error has occurred in Vionis Restaurant Solution - Admin panel:\n\n {}'
                        .format('{} at \n\n Link to detail: {}'.format(error, '#paste ISPmanager link to this#')))
    return render_template('500.html', base_template=appbuilder.base_template, appbuilder=appbuilder), 500


class MyView(BaseView):
    route_base = "/my"

    @expose('/test')
    def method1(self):
        e = 3/0
        return 'qwe'


appbuilder.add_view_no_menu(RestaurantSubjectModelView)
appbuilder.add_view_no_menu(RestaurantModelView)
appbuilder.add_view_no_menu(MyView())

appbuilder.add_view(RestaurantMultiplyModelView, "RestaurantsMulty", category='RestaurantsManage',
                    category_icon='fa-cutlery', category_label=u'Рестораны',
                    icon="fa-th-list", label=u"Список ресторанов")

appbuilder.add_view(UserRestaurantModelView, "UserRestaurants", category='RestaurantsManage',
                    category_icon='fa-cutlery', category_label=u'Рестораны',
                    icon="fa-user-circle-o", label=u"Рестораны пользователей")
