# coding=utf-8
from flask_appbuilder.models.mongoengine.interface import MongoEngineInterface

from app import appbuilder
from app.models.product_models import ProductType
from app.rest_permission import BaseRestaurantViewModel


class ProductTypeModelView(BaseRestaurantViewModel):
    datamodel = MongoEngineInterface(ProductType)

    list_title = u'Типы продуктов'
    base_order = ('name', 'asc')

    def pre_init(self):
        self.label_columns['name'] = u'Название'
        self.list_columns.insert(0, 'name')


appbuilder.add_view(ProductTypeModelView, "ProductTypes", icon="fa-th-list", label=u"Типы продуктов",
                    category='Products', category_icon='fa-shopping-basket', category_label=u'Продукты')
