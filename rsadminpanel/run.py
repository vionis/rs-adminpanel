# coding=utf-8
import logging
from app import app

# set needed encoding
import sys
reload(sys)
sys.setdefaultencoding('utf-8')

app.run(host='0.0.0.0', port=8080, debug=False)

