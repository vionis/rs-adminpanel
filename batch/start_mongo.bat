@echo off
set databasepath=d:\mydev\rs-container\database\
cd D:\mydev\rs-container\MongoDB\Server\4.0\bin

mongod.exe --dbpath=%databasepath% --port=27017 --bind_ip=127.0.0.1
