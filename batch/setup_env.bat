@echo off
if exist ../env (
echo env installed 
) else (
call :setupfunc
)
exit /B 0

:setupfunc
    set target2xpy=c:\Python27\python.exe
    virtualenv --python=%target2xpy% ../env
    cd ..\env\Scripts\
    pip2.7.exe install flask-appbuilder==1.12.0
    pip2.7.exe install flask-mongoengine==0.9.5
    pip2.7.exe install vk==2.0.2
exit /B 0


